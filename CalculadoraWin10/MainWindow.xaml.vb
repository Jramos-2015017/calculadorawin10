﻿Class MainWindow
    Private operacion As String = ""

    WithEvents evaluar As New Evaluator
    Dim strNum As String
    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

        Dim cadena As String = CType(sender, Button).Content

        If Not (cadena.Equals("=") Or cadena.Equals("√") Or cadena.Equals("x²") Or cadena.Equals("CE") Or
            cadena.Equals("C") Or cadena.Equals("±") Or cadena.Equals("⌫") Or cadena.Equals("1/x")) Then
            operacion = operacion + cadena
        End If

        If Not (cadena.Equals("=") Or cadena.Equals("±") Or cadena.Equals("√") Or cadena.Equals("CE") Or cadena.Equals("x²") Or cadena.Equals("C") Or cadena.Equals("⌫") Or
            cadena.Equals("+") Or cadena.Equals("1/x") Or cadena.Equals("-") Or cadena.Equals("*") Or cadena.Equals("/")) Then
            strNum = strNum + cadena
        End If

        Resultados.Text = strNum
        Dim result = Resultados.Text
        Select Case (CType(sender, Button).Content)
            Case "CE"
                Resultados.Text = 0
                strNum = ""
            Case "C"
                operacion = ""
                strNum = ""
                Expresion.Text = ""
                Resultados.Text = ""
                Advertencia.Text = ""
            Case "⌫"
                Try
                    If (operacion <> "" Or Resultados.Text <> "") Then
                        operacion = operacion.Remove(operacion.Length - 1)
                        strNum = strNum.Remove(strNum.Length - 1)
                        Resultados.Text = Resultados.Text.
                            Remove(Resultados.Text.Length - 1)
                        Advertencia.Text = Nothing
                    End If
                Catch ex As Exception
                End Try
            Case "%"
                Try
                    If (operacion <> "") Then
                        strNum = ""
                        Advertencia.Text = Nothing
                        Expresion.Text = operacion
                        Resultados.Text = evaluar.Eval(Expresion.Text)
                    Else
                        Expresion.Text = "0"
                        Resultados.Text = Nothing
                    End If
                Catch ex As Exception
                End Try
            Case "x²"
                Try
                    If (operacion <> "" Or Resultados.Text <> "") Then
                        Expresion.Text = "sqr" + "(" + operacion + ")"
                        strNum = ""
                        Advertencia.Text = Nothing
                        Resultados.Text = cuadrado(ex())
                        operacion = operacion.Replace(operacion, cuadrado(ex()))
                        almacenar(operacion)
                    Else
                        Expresion.Text = "sqr(0)"
                        Resultados.Text = 0

                    End If
                Catch ex As Exception
                End Try

            Case "1/x"
                Try
                    If (operacion <> "") Then
                        strNum = ""
                        Advertencia.Text = Nothing
                        Expresion.Text = "1/" + "(" + operacion + ")"
                        Resultados.Text = fraccion(ex())
                        operacion = operacion.Replace(operacion, fraccion(ex()))
                        almacenar(operacion)
                    Else
                        Expresion.Text = "1/(0)"
                        Advertencia.Text = "No puede dividir entre cero"

                    End If
                Catch ex As Exception
                End Try

            Case "√"
                Try
                    If (operacion <> "") Then
                        strNum = ""
                        Advertencia.Text = Nothing
                        Expresion.Text = "√" + "(" + operacion + ")"
                        Resultados.Text = raices(ex())
                        operacion = operacion.Replace(operacion, raices(ex()))
                        almacenar(operacion)
                    Else
                        Expresion.Text = "√(0)"
                        Resultados.Text = 0

                    End If
                Catch ex As Exception
                End Try

            Case "±"
                strNum = ""
                Resultados.Text = "-" & operacion
                operacion = Resultados.Text
                Expresion.Text = operacion
                Advertencia.Text = Nothing
            Case "7"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "8"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "9"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "*"
                Advertencia.Text = Nothing
                strNum = ""
                Expresion.Text = operacion
            Case "4"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "5"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "6"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "-"
                Advertencia.Text = Nothing
                strNum = ""
                Expresion.Text = operacion
            Case "/"
                Advertencia.Text = Nothing
                strNum = ""
                Expresion.Text = operacion
            Case "1"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "2"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "3"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "+"
                Advertencia.Text = Nothing
                strNum = ""
                Expresion.Text = operacion
            Case "0"
                Advertencia.Text = Nothing
                Resultados.Text = strNum
            Case "."

            Case "="
                Try
                    If (operacion <> "" Or Resultados.Text <> "") Then
                        Advertencia.Text = Nothing
                        Expresion.Text = ""
                        operacion.Replace(".", ",")
                        Resultados.Text = evaluar.Eval(operacion).ToString
                        operacion = Resultados.Text
                        ex()
                    Else
                        Expresion.Text = Nothing
                        Resultados.Text = 0

                    End If
                Catch ex As Exception
                End Try

        End Select
    End Sub
    Public Function cuadrado(ByVal operacion As Double) As Double
        Return operacion * operacion
    End Function

    Public Function fraccion(ByVal operacion As Double) As Double
        Return 1 / operacion
    End Function

    Public Function raices(ByVal operacion As Double) As Double
        Return Math.Sqrt(operacion)
    End Function

    Public Function almacenar(ByVal op As Double) As Double
        Dim resultado = operacion

        Return resultado
    End Function
    Public Function ex()
        Dim op = almacenar(operacion)
        Return op
    End Function

End Class
Public Class Evaluator

    Private mParser As New parser(Me)
    Private mExtraFunctions As Object

    Private Enum eTokenType
        none
        end_of_formula
        operator_plus
        operator_minus
        operator_mul
        operator_div
        open_parenthesis
        close_parenthesis
        value_number
        dot
        comma
        operator_percent
    End Enum

    Private Enum ePriority
        none = 0
        [concat] = 1
        [or] = 2
        [and] = 3
        [not] = 4
        equality = 5
        plusminus = 6
        muldiv = 7
        percent = 8
        unaryminus = 9
    End Enum

    Public Class parserexception
        Inherits Exception

        Friend Sub New(ByVal str As String)
            MyBase.New(str)
        End Sub

    End Class

    Private Class tokenizer

        Private mString As String
        Private mLen As Integer
        Private mPos As Integer
        Private mCurChar As Char
        Public startpos As Integer
        Public type As eTokenType
        Public value As New System.Text.StringBuilder
        Private mParser As parser
        Sub op()
            Console.WriteLine("Escriba la operacion")
            Dim operacion As String
            Do
                operacion = Console.ReadLine()
                If operacion = "end" Then Exit Do
            Loop
        End Sub
        Sub New(ByVal Parser As parser, ByVal str As String)
            mString = str
            mLen = str.Length
            mPos = 0
            mParser = Parser
            NextChar()
        End Sub
        Sub NextChar()
            If mPos < mLen Then
                mCurChar = mString.Chars(mPos)
                If mCurChar = Chr(147) Or mCurChar = Chr(148) Then
                    mCurChar = """"c
                End If
                mPos += 1
            Else
                mCurChar = Nothing
            End If
        End Sub

        Public Function IsOp() As Boolean
            Return mCurChar = "+"c _
               Or mCurChar = "-"c _
               Or mCurChar = "%"c _
               Or mCurChar = "/"c _
               Or mCurChar = "("c _
               Or mCurChar = ")"c _
               Or mCurChar = "."c
        End Function

        Public Sub NextToken()
            startpos = mPos
            value.Length = 0
            type = eTokenType.none
            Do
                Select Case mCurChar
                    Case Nothing
                        type = eTokenType.end_of_formula
                    Case "0"c To "9"c
                        ParseNumber()
                    Case "-"c
                        NextChar()
                        type = eTokenType.operator_minus
                    Case "+"c
                        NextChar()
                        type = eTokenType.operator_plus
                    Case "*"c
                        NextChar()
                        type = eTokenType.operator_mul
                    Case "/"c
                        NextChar()
                        type = eTokenType.operator_div
                    Case "("c
                        NextChar()
                        type = eTokenType.open_parenthesis
                    Case ")"c
                        NextChar()
                        type = eTokenType.close_parenthesis
                    Case "%"c
                        NextChar()
                        type = eTokenType.operator_percent
                    Case ","c
                        NextChar()
                        type = eTokenType.comma
                    Case "."c
                        NextChar()
                        type = eTokenType.dot
                    Case Chr(0) To " "c
                    Case Else

                End Select
                If type <> eTokenType.none Then Exit Do
                NextChar()
            Loop
        End Sub


        Public Sub ParseNumber()
            type = eTokenType.value_number
            While mCurChar >= "0"c And mCurChar <= "9"c
                value.Append(mCurChar)
                NextChar()
            End While
            If mCurChar = "."c Then
                value.Append(mCurChar)
                NextChar()
                While mCurChar >= "0"c And mCurChar <= "9"c
                    value.Append(mCurChar)
                    NextChar()
                End While
            End If
        End Sub

        Public Sub ParseString(ByVal InQuote As Boolean)
            Dim OriginalChar As Char
            If InQuote Then
                OriginalChar = mCurChar
                NextChar()
            End If
            If InQuote Then
            End If
        End Sub

    End Class

    Private Class parser
        Dim tokenizer As tokenizer
        Private mEvaluator As Evaluator
        Sub New(ByVal evaluator As Evaluator)
            mEvaluator = evaluator
        End Sub

        Friend Function ParseExpr(ByVal Acc As Object, ByVal priority As ePriority) As Object
            Dim ValueLeft, valueRight As Object
            Do
                Select Case tokenizer.type
                    Case eTokenType.operator_minus
                        tokenizer.NextToken()
                        ValueLeft = ParseExpr(0, ePriority.unaryminus)
                        If TypeOf ValueLeft Is Double Then
                            ValueLeft = -DirectCast(ValueLeft, Double)
                        Else
                        End If
                        Exit Do
                    Case eTokenType.operator_plus
                        tokenizer.NextToken()

                        Exit Do
                        Exit Do
                    Case eTokenType.value_number
                        ValueLeft = Double.Parse(tokenizer.value.ToString)
                        tokenizer.NextToken()
                        Exit Do
                    Case eTokenType.open_parenthesis
                        tokenizer.NextToken()
                        ValueLeft = ParseExpr(0, ePriority.none)
                        If tokenizer.type = eTokenType.close_parenthesis Then
                            tokenizer.NextToken()
                            Exit Do
                        Else

                        End If
                    Case Else
                        Exit Do
                End Select
            Loop
            Do
                Dim tt As eTokenType
                tt = tokenizer.type
                Select Case tt
                    Case eTokenType.end_of_formula
                        Return ValueLeft
                    Case eTokenType.value_number
                        Exit Function
                    Case eTokenType.operator_plus
                        If priority < ePriority.plusminus Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.plusminus)
                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                ValueLeft = CDbl(ValueLeft) + CDbl(valueRight)
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is Double) Then
                                ValueLeft = CDate(ValueLeft).AddDays(CDbl(valueRight))
                            ElseIf (TypeOf ValueLeft Is Double And TypeOf valueRight Is DateTime) Then
                                ValueLeft = CDate(valueRight).AddDays(CDbl(ValueLeft))
                            Else
                                ValueLeft = ValueLeft.ToString & valueRight.ToString
                            End If
                        Else
                            Exit Do
                        End If
                    Case eTokenType.operator_minus
                        If priority < ePriority.plusminus Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.plusminus)
                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                ValueLeft = CDbl(ValueLeft) - CDbl(valueRight)
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is Double) Then
                                ValueLeft = CDate(valueRight).AddDays(-CDbl(ValueLeft))
                            ElseIf (TypeOf ValueLeft Is DateTime And TypeOf valueRight Is DateTime) Then
                                ValueLeft = CDate(ValueLeft).Subtract(CDate(valueRight)).TotalDays
                            Else
                            End If
                        Else
                            Exit Do
                        End If
                    Case eTokenType.operator_mul, eTokenType.operator_div
                        If priority < ePriority.muldiv Then
                            tokenizer.NextToken()
                            valueRight = ParseExpr(ValueLeft, ePriority.muldiv)

                            If TypeOf ValueLeft Is Double _
                        And TypeOf valueRight Is Double Then
                                If tt = eTokenType.operator_mul Then
                                    ValueLeft = CDbl(ValueLeft) * CDbl(valueRight)
                                Else
                                    ValueLeft = CDbl(ValueLeft) / CDbl(valueRight)
                                End If
                            Else
                            End If
                        Else
                            Exit Do
                        End If
                    Case Else
                        Exit Do
                End Select
            Loop

            Return ValueLeft
        End Function

        Public Function Eval(ByVal str As String) As Object
            If Len(str) > 0 Then
                tokenizer = New tokenizer(Me, str)
                tokenizer.NextToken()
                Dim res As Object = ParseExpr(Nothing, ePriority.none)

                If tokenizer.type = eTokenType.end_of_formula Then

                    Return res
                Else

                End If
            End If
        End Function

        Public Function EvalString(ByVal str As String) As String
            If Len(str) > 0 Then
                tokenizer = New tokenizer(Me, str)
                tokenizer.ParseString(False)
                Return tokenizer.value.ToString
            Else
                Return String.Empty
            End If
        End Function

    End Class
    Public Function EvalDouble(ByRef formula As String) As Double
        Dim res As Object = Eval(formula)
        If TypeOf res Is Double Then
            Return CDbl(res)
        Else
        End If
    End Function

    Public Function Eval(ByVal str As String) As Object
        str = str.Replace(" ", "")
        str = str.Replace(".", ",")
        Return mParser.Eval(str)
    End Function

    Public Function EvalString(ByVal str As String) As String
        Return mParser.EvalString(str)
    End Function
    Event GetVariable(ByVal name As String, ByRef value As Object)
End Class